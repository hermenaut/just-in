class CreateStaticAssets < ActiveRecord::Migration[5.1]
  def change
    create_table :static_assets do |t|

      t.timestamps
    end
  end
end
