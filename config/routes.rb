Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  
  root 'static_assets#home'

  get '/stage', to: 'static_assets#stage'
  get '/teststage', to: 'static_assets#teststage'


  get '/stages/one', to: 'stages#one'
  get '/stages/two', to: 'stages#two'

  resources :static_assets



end
